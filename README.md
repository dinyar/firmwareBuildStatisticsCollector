# uGMTbuildStatisticsCollector

Project which should allow to collect build statistics for the uGMTfirmware.

We store build data and provide both automatically generated plots as well as a Jupyter notebook which can be used to further analyse the data.

## Setup

On the intended host the following needs to be set up for a local user:
- Jupyter
    - Needs:
    - python34-pip
    - python34-devel
    - gcc-c++
    - Open firewall port: `sudo firewall-cmd --zone=public --add-port=8888/tcp --permanent` and `sudo firewall-cmd --reload`
    - Create password hash, allow access from remote machines
        - Ideally also create certificate
    - Run `jupyter notebook` in folder with SQLite file 
- Python dataset 
- Flask
- Gunicorn
    - Run with `gunicorn -w 4 -b :5000 StatCollector:app`
    - Needs port 5000 open (as above)

### Systemd services

We run both Jupyter and the Collector as services on our machine.

For Jupyter this is done as follows:

```
[Unit]
Description=Jupyter Workplace

[Service]
Type=simple
PIDFile=/run/jupyter.pid
ExecStart=/home/ugmt_stats_collector/.local/bin/jupyter notebook
User=ugmt_stats_collector
Group=ugmt_stats_collector
WorkingDirectory=/home/ugmt_stats_collector/workspace
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target
```
in `/etc/systemd/system/jupyter.service`

And for the Collector with:

```
[Unit]
Description=uGMT build statistic collector

[Service]
Type=simple
PIDFile=/run/uGMTbuildStatCollector.pid
ExecStart=/home/ugmt_stats_collector/.local/bin/gunicorn -w 1 -b :5000 StatCollector:app
User=ugmt_stats_collector
Group=ugmt_stats_collector
WorkingDirectory=/home/ugmt_stats_collector/uGMTbuildStatisticsCollector
Restart=always
RestartSec=10

[Install]
WantedBy=multi-user.target
```
in `/etc/systemd/system/ugmt_stat_collector.service`.

We run it with `sudo systemctl restart [...].service` and make sure it runs at reboot with `sudo systemctl enable [...].service`.
