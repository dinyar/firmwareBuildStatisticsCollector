#!/bin/env python3
import dataset
from flask import Flask, redirect, url_for, request, render_template, make_response, flash, session

app = Flask(__name__)
app.secret_key = "Foi%BgDFdf4eJGHBf.m,4"
db_string = 'sqlite:///ugmt_build_stats.db'

# TODO: Might be interesting to have a retrieval too..

@app.route('/insert', methods=['POST'])
def insert():
    branch = request.form.get('branch') # Will make this optional because an existing commit in the uGMTfirmware repo doesn't use it.
    commit_hash = request.form['chash']
    commit_time = request.form['ctime']
    synth_luts_cnt = request.form['synth_luts_cnt']
    synth_regs_cnt = request.form['synth_regs_cnt']
    synth_bram_cnt = request.form['synth_bram_cnt']
    synth_luts_per = request.form['synth_luts_per']
    synth_regs_per = request.form['synth_regs_per']
    synth_bram_per = request.form['synth_bram_per']
    impl_luts_cnt = request.form['impl_luts_cnt']
    impl_regs_cnt = request.form['impl_regs_cnt']
    impl_bram_cnt = request.form['impl_bram_cnt']
    impl_luts_per = request.form['impl_luts_per']
    impl_regs_per = request.form['impl_regs_per']
    impl_bram_per = request.form['impl_bram_per']
    worst_setup_slack = request.form['WNS']
    worst_hold_slack = request.form['WHS']
    synth_time = request.form['synth_time']
    impl_time = request.form['impl_time']

    # For now just sticking all that into a row in the table
    db = dataset.connect(db_string)
    table = db.create_table('logic_usage',
                            primary_id='chash',
                            primary_type=db.types.text)
    return str(table.insert(dict(chash=commit_hash,
                                 branch=branch,
                                 ctime=commit_time,
                                 synth_luts_cnt=synth_luts_cnt,
                                 synth_regs_cnt=synth_regs_cnt,
                                 synth_bram_cnt=synth_bram_cnt,
                                 synth_luts_per=synth_luts_per,
                                 synth_regs_per=synth_regs_per,
                                 synth_bram_per=synth_bram_per,
                                 impl_luts_cnt=impl_luts_cnt,
                                 impl_regs_cnt=impl_regs_cnt,
                                 impl_bram_cnt=impl_bram_cnt,
                                 impl_luts_per=impl_luts_per,
                                 impl_regs_per=impl_regs_per,
                                 impl_bram_per=impl_bram_per,
                                 worst_setup_slack=worst_setup_slack,
                                 worst_hold_slack=worst_hold_slack,
                                 synth_time=synth_time,
                                 impl_time=impl_time
                                 )))+"\n"

if __name__ == '__main__':
    app.run()

